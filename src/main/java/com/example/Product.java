package com.example;



import javax.persistence.*;
import java.util.List;

/**
 * Created by hasan on 3/29/17.
 */

@Entity
@Table(name = "BOLT_PRODUCTS")
public class Product extends BaseEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;
    private String description;

//    @ManyToOne
//    @JoinColumn(name = "OWNER_ID")
//    private Member owner;

    @OneToMany(mappedBy = "product")
    private List<ProductBacklog> backlogs;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

//    public Member getOwner() {
//        return owner;
//    }
//
//    public void setOwner(Member owner) {
//        this.owner = owner;
//    }

    public List<ProductBacklog> getBacklogs() {
        return backlogs;
    }

    public void setBacklogs(List<ProductBacklog> backlogs) {
        this.backlogs = backlogs;
    }
}
