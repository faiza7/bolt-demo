package com.example;


import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "BOLT_USER_STORIES")
public class UserStory extends BaseEntity{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "TASK_ID_GENERATOR")
    @SequenceGenerator(name = "TASK_ID_GENERATOR",
            allocationSize = 1,
            initialValue = 1,
            sequenceName = "SEQ_TASK_ID")
    private Long id;
    private String referenceId;
    private String description;
    @ManyToOne
    @JoinColumn(name = "PRODUCT_ID")
    private Product product;
//    private TaskStatus status;
    @OneToMany
    @JoinColumn(name = "TASK_ID")
//    private List<Comment> comments;
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getReferenceId() {
        return referenceId;
    }
    public void setReferenceId(String referenceId) {
        this.referenceId = referenceId;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public Product getProduct() {
        return product;
    }
    public void setProduct(Product product) {
        this.product = product;
    }
//    public TaskStatus getStatus() {
//        return status;
//    }
//    public void setStatus(TaskStatus status) {
//        this.status = status;
//    }
//    public List<Comment> getComments() {
//        return comments;
//    }
//    public void setComments(List<Comment> comments) {
//        this.comments = comments;
//    }
}