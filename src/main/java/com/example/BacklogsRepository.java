package com.example;



import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BacklogsRepository extends CrudRepository<ProductBacklog, Long> {
    ProductBacklog findById(Long id);

    List<ProductBacklog> findAll();

    ProductBacklog findByName(@Param("name") String name);
}
