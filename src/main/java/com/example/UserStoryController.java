package com.example;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Faza on 4/12/2017.
 */
@RestController
public class UserStoryController {
    @Autowired
    UserStoryRepository userStoryRepository;

    @RequestMapping(value="/userstories",method = RequestMethod.GET)
    public ResponseEntity<List<UserStory>> getAllUserStory() {
        List<UserStory> userStory = userStoryRepository.findAll();
        return new ResponseEntity<List<UserStory>>(userStory, HttpStatus.OK);
    }
    @RequestMapping(value="/userstories",method = RequestMethod.POST)
    public ResponseEntity<UserStory> addUserStory(@RequestBody UserStory userStory){
        userStoryRepository.save(userStory);
        return new ResponseEntity<UserStory>(userStory,HttpStatus.OK);
    }
    @RequestMapping(value="/userstories/{id}",method = RequestMethod.GET)
    public ResponseEntity<UserStory> getUserStory(@PathVariable("id") Long id){
        UserStory userStory = userStoryRepository.findById(id);
        return new ResponseEntity<UserStory>(userStory,HttpStatus.OK);
    }


}
