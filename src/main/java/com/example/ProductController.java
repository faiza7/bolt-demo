package com.example;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController

public class ProductController {

    @Autowired ProductRepository productRepository;

    @RequestMapping(value="/product",method = RequestMethod.GET)
    public ResponseEntity<List<Product>> getAllProduct() {
        List<Product> product = productRepository.findAll();
        return new ResponseEntity<List<Product>>(product,HttpStatus.OK);
    }
    @RequestMapping(value="/product",method = RequestMethod.POST)
     public ResponseEntity<Product> addProduct(@RequestBody Product product){
        productRepository.save(product);
    return new ResponseEntity<Product>(product,HttpStatus.OK);
    }
    @RequestMapping(value="/product/{id}",method = RequestMethod.GET)
        public ResponseEntity<Product> getAccount(@PathVariable("id") Long id){
        Product product;
        product = productRepository.findById(id);
        return new ResponseEntity<Product>(product,HttpStatus.OK);
    }
    @RequestMapping(value="/product/search")

    public ResponseEntity<Product> product(@RequestParam(value="name") String name) {
        Product product = productRepository.findByName(name);
        return new ResponseEntity<Product>(product,HttpStatus.OK);

    }




}
