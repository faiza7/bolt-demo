package com.example;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

/**
 * Created by hasan on 3/29/17.
 */

@Entity
@Table(name = "BOLT_SPRINTS")
public class Sprint extends BaseEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SPRINT_ID_GENERATOR")
    @SequenceGenerator(name = "SPRINT_ID_GENERATOR",
            allocationSize = 1,
            initialValue = 1,
            sequenceName = "SEQ_SPRINT_ID")
    private Long id;
    private String name;
    private LocalDate startDate;
    private LocalDate endDate;

    @OneToMany
    @JoinTable(name = "BOLT_SPRINT_TASKS",
        joinColumns = @JoinColumn(name = "SPRINT_ID"),
        inverseJoinColumns = @JoinColumn(name = "SPRINT_TASK_ID")
    )
//    private List<SprintTask> sprintTasks;

    @Enumerated(EnumType.STRING)
//    private SprintStatus status;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

//    public List<SprintTask> getSprintTasks() {
//        return sprintTasks;
//    }
//
//    public void setSprintTasks(List<SprintTask> sprintTasks) {
//        this.sprintTasks = sprintTasks;
//    }

//    public SprintStatus getStatus() {
//        return status;
//    }
//
//    public void setStatus(SprintStatus status) {
//        this.status = status;
//    }
}
