package com.example;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

/**
 * Created by Faza on 4/12/2017.
 */
@RestController

public class BacklogsController {
    @Autowired
    BacklogsRepository backlogsRepository;

    @RequestMapping(value="/backlogs", method= RequestMethod.POST)

    public ResponseEntity<ProductBacklog> addBacklogs(@RequestBody ProductBacklog backlogs) {
        backlogsRepository.save(backlogs);
        return new ResponseEntity<ProductBacklog>(backlogs,HttpStatus.OK);

    }
    @RequestMapping(value="/backlogs/{id}",method = RequestMethod.GET)
    public ResponseEntity<ProductBacklog> getBacklogs(@PathVariable("id") Long id) {
        ProductBacklog backlogs;
        backlogs = backlogsRepository.findById(id);
        return new ResponseEntity<ProductBacklog>(backlogs, HttpStatus.OK);
    }
    @RequestMapping(value="/backlogs",method = RequestMethod.GET)
    public ResponseEntity<List<ProductBacklog>> getAllBacklogs() {
        List<ProductBacklog> backlogs = backlogsRepository.findAll();
        return new ResponseEntity<List<ProductBacklog>>(backlogs,HttpStatus.OK);
    }
}