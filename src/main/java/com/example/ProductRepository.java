package com.example;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepository extends CrudRepository<Product, Long>{
    Product findById(Long id);
    List<Product> findAll();
    Product findByName(@Param("name") String name);


}
