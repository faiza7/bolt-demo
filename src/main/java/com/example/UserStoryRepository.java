package com.example;



import org.springframework.data.repository.CrudRepository;

import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface UserStoryRepository extends CrudRepository<UserStory, Long> {
    UserStory findById(Long id);

    List<UserStory> findAll();

}