//package com.example;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//import java.io.Serializable;
//import java.util.List;
//
//
//@Service
//public class DefaultBoltService implements BoltService {
//
//    @Autowired
//    private ProductRepository productRepository;
//
//    @Override
//    public Product save(Product entity) {
//        return productRepository.save(entity);
//    }
//
//    @Override
//    public Product getById(Serializable id) {
//        return productRepository.findOne((Long) id);
//    }
//
//    @Override
//    public List<Product> getAll() {
//        return productRepository.findAll();
//    }
//
//    @Override
//    public void delete(Serializable id) {
//        productRepository.delete((Long) id);
//    }
//
//}
//    @Autowired
//    private BacklogsRepository backlogsRepository;
//
//    @Override
//    public ProductBacklog save(ProductBacklog entity) {
//        return backlogsRepository.save(entity);
//    }
//    @Override
//    public ProductBacklog getById(Serializable id) {
//        return backlogsRepository.findOne((Long) id);
//    }
//
//    @Override
//    public List<ProductBacklog> getAll() {
//        return backlogsRepository.findAll();
//    }
//
//    @Override
//    public void delete(Serializable id) {
//        backlogsRepository.delete((Long) id);
//    }
//}