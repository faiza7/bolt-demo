package com.example;



import javax.persistence.*;
        import java.util.ArrayList;
        import java.util.List;

@Entity
@Table(name = "BOLT_PRODUCT_BACKLOGS")
public class ProductBacklog extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "PRODUCT_BACKLOG_ID_GENERATOR")
    @SequenceGenerator(name = "PRODUCT_BACKLOG_ID_GENERATOR",
            allocationSize = 1,
            initialValue = 1,
            sequenceName = "SEQ_PRODUCT_BACKLOG_ID")
    private Long id;
    private String name;
    @Lob
    private String description;
    @OneToMany
    @JoinTable(name = "BOLT_PRODUCT_BACKLOG_STORIES",
            joinColumns = @JoinColumn(name = "BACKLOG_ID"),
            inverseJoinColumns = @JoinColumn(name = "STORY_ID")
    )
    private List<UserStory> userStories;
    @ManyToOne
    @JoinColumn(name = "PRODUCT_IDs")
    private Product product;
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public List<UserStory> getUserStories() {
        if(userStories == null) userStories = new ArrayList<>();
        return userStories;
    }
    public void setUserStories(List<UserStory> userStories) {
        this.userStories = userStories;
    }
    public Product getProduct() {
        return product;
    }
    public void setProduct(Product product) {
        this.product = product;
    }
    public void addUserStory(UserStory userStory){
        getUserStories().add(userStory);
    }
    public void removeUserStory(UserStory userStory){
        getUserStories().remove(userStory);
    }
}